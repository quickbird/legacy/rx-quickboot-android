package com.quickbirdstudios.quickboot.extension

import com.quickbirdstudios.quickboot.DisposeBag
import com.quickbirdstudios.quickboot.Optional
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.toObservable

/**
 * Created by sebastiansellmair on 06.11.17.
 */
fun <G : Iterable<T>, T : Any, U : Any> Observable<G>.mapIterable(block: (T) -> U)
        : Observable<List<U>> {
    return this.map {
        it.map(block)
    }
}

fun <T : Any> Observable<Optional<T>>.filterNotNull(): Observable<T> {
    return this.flatMap {
        val unboxed = it.raw
        val list = mutableListOf<T>()
        if (unboxed != null) list.add(unboxed)
        list.toObservable()
    }
}

/**
 * Filters null values using the [filterNotNull] function.
 */
fun <T : Any, U : Any> Observable<T>.mapNotNull(block: (T) -> U?): Observable<U> = this
        .map { block(it).box() }
        .filterNotNull()


fun <T> Observable<T>.onErrorComplete(): Observable<T> = this.onErrorResumeNext(Observable.empty())

fun <T : Disposable> T.disposedBy(disposeBag: DisposeBag): T {
    disposeBag.add(this)
    return this
}



