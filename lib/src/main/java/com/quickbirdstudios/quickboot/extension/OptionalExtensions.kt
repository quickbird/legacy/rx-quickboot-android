package com.quickbirdstudios.quickboot.extension

import com.quickbirdstudios.quickboot.Optional

/**
 * Created by sebastiansellmair on 14.02.18.
 */
fun <T> T?.box(): Optional<T> = Optional(this)