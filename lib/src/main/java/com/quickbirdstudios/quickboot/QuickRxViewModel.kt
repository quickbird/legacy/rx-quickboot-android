@file:Suppress("unused")

package com.quickbirdstudios.quickboot

import android.support.annotation.CallSuper
import com.quickbirdstudios.quickboot.architecture.QuickViewModel

/**
 * Created by sebastiansellmair on 07.11.17.
 */
open class QuickRxViewModel<out Inputs, out Outputs> : QuickViewModel<Inputs, Outputs>() {

    @Suppress("MemberVisibilityCanPrivate")
    val disposeBag = DisposeBag.create()


    @CallSuper
    override fun onCleared() {
        super.onCleared()
        disposeBag.dispose()
    }
}