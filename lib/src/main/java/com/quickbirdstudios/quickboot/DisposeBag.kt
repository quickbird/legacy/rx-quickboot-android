package com.quickbirdstudios.quickboot

import io.reactivex.disposables.Disposable

/**
 * Created by sebastiansellmair on 07.11.17.
 */
interface DisposeBag: Disposable {
    fun add(disposable: Disposable)
    fun remove(disposable: Disposable)
    companion object {
        fun create(): DisposeBag = BasicDisposeBag()
        internal var verbose = false
    }
}

